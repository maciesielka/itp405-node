(function(){

	var express = require('express');
	var ejs     = require('ejs');

	var app		= express();

	var Dvd 	= require('./Dvd');

	app.set('view engine', 'ejs');

	app.get('/', function(request, response){
		response.render('index', {

		});

		response.end();
	});

	app.get('/dvds', function(request, response){

		Dvd.findAll({
			title: { like: '%' + request.query.title + '%' },
			award: request.query.award
		}).then(function(results){
			response.render('dvds', {
				dvds: results
			});

			response.end();
		}, function(error){
			console.log(error);

			response.end();
		});

		//response.end();
	});

	app.listen(3000, function(){
		console.log("Listening on localhost:3000");
	});

})();
