(function(){

	var Sequelize 	= require("sequelize");
	var sqlize 		= require("./database");

	var Dvd = sqlize.define('dvd', {
		title: {
			field:  'title',
			type: 	Sequelize.STRING
		},

		award: {
			field: 	'award',
			type:	Sequelize.STRING
		}
	}, { //options object
		timestamps: false
	});

	module.exports = Dvd;

})();
